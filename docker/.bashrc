PS1='[\u@rv-multi \W]\$ '

export TPATH=/home/user/rv-multi/tools
export AJ=$TPATH/aspectj
export RVMON=$TPATH/RV-Monitor/bin
export JMOP=$TPATH/jmop/javamop
#export RVPRED=$TPATH/RV-Predict/Java/bin
export RVPRED_HOME=$TPATH/RV-Predict/Java
export RVPRED=$TPATH/RV-Predict/bin

export MARQ=$TPATH/Marq/marq-1.1.jar:$TPATH/Marq/qea-1.0.jar:$TPATH/Marq/qeaProperties-1.0.jar
export LARVA=$TPATH/Larva/larva.jar

# Alpine bug, doesnt have javac in path
export JP=/usr/lib/jvm/default-jvm/bin

export ASPECTJ_HOME=$AJ

export PATH=$AJ/bin:$RVPRED:$RVMON:$JMOP/bin:$JMOP/lib:$JP:$PATH
