---
title: Docker Image
---

This folder builds a docker image with all needed dependencies to run build and execute all artifacts.
Additionally, the docker provides a server to browse the `README.md` files.

Firstly, make sure you are in the [docker](.) folder:
```bash
cd docker
```

## Creating and Running the Image ##

The following instructions manipulate the docker container, please make sure to have **root** privileges (if necessary) for docker commands (i.e., execute **sudo** if needs be).

### Creating The Docker Image

You can either download the pre-built image or build it yourself using the [Dockerfile](Dockerfile).

* **OPTION 1**: Download the pre-built image (~93MB)

```
# make fetch
```

If the image loaded correctly, delete the downloaded archive as it is no longer necessary.

```
# rm docker-rv.gz
```

* **OPTION 2**: Build the docker image  (~ 30 minutes)

```
# make build
```

### Running The Docker Image

* Bind all the artifacts to the running docker image and launch a shell in the docker container

```
# make run
```

* Proceed to: [after running](#after-running).

## After Running

* It is now possible to use a regular file explorer and programs to browse the files in the directory in parallel, changes done in the docker reflect on the files directly (outside docker).

> **Note:** The docker image mounts all the artifacts (as volumes) in read-write mode, changes done in the docker container will reflect in the original folder.

* You can now also browse the documentation using a browser using the pre-rendered files: [http://localhost:8050/](http://localhost:8050)




## Changing the Artifacts Mountpoints ##

The default artifacts are mounted from the parent directory, i.e.: [`..`](/), the docker image will mount the following:


If you wish to use a different workdir than  [`..`](../), it must be an absolute path in the form of `/path/to/workdir`.


To do so, simply provide the path with `WD` as follows:

```
# make run WD=/path/to/workdir
```


## Deleting the Image ##

Once done with using the docker image, delete the created docker processes and images relevant to THEMIS by running:

```
# make clean
```
