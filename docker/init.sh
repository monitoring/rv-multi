#!/bin/bash
TITLE=rv-multithreaded
USER=user
ART=rv-multi
HOST_MNT=/home/$USER/$ART

# BSD or Linux?
stat --help  1>/dev/null 2>&1
if [ $? != 0 ]; then
  STATF="stat -f "
else
  STATF="stat -c "
fi

# Set up user/group
GROUPID=`$STATF %g $HOST_MNT`
USERID=`$STATF %u $HOST_MNT`

source /home/$USER/.bashrc

#Python3
#(cd /home/$USER/$ART && python -m http.server --bind 0.0.0.0 8050 2>/dev/null 1>/dev/null) &

#Python2
(cd /home/$USER/$ART && python2 -m SimpleHTTPServer 8050 2>/dev/null 1>/dev/null) &

echo
echo "---- $TITLE ------"

echo "root:root" | chpasswd
echo ""
echo " root's password is 'root'"

if [[ $USERID -eq 0 && $GROUPID -eq 0 ]]
then
  echo "# Warning: Directory '$HOST_MNT' is not mounted"
  echo "# Running as root"
  USER=root
else
  adduser --uid $USERID -D  -s /bin/bash $USER
  chown -R $USER:$USER /home/user
fi

echo ""
echo ""
echo "--------------------------------"
echo "   Browse the README files:"
echo "   http://localhost:8050/"
echo "--------------------------------"
echo
exec su $USER
