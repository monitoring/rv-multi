---
title: Tutorial on RV for Multithreaded Programs
---


## Tutorial: Can We Monitor All Multithreaded Programs?

In this tutorial, we explore some of the RV tools that explicitly handle multithreaded programs by looking at the interplay between program executions and specification formalisms.


### Reading the README.md Files ##
All README.md files are pre-rendered as HTML for convenience. 
In each directory (including this one), the `index.html` file corresponds to a rendered `README.md`.


### Setting Up

A [Docker](docker/) is provided that includes the tools and the necessary environment to run the tutorial.
For more information check the [docker](docker) folder.

If you opt to **not** use the provided docker, ensure you have Java 8 (jdk-1.8) installed, and add the needed environment variables found in [.bashrc](docker/.bashrc). Tools are provided in the folder [tools](tools/).

### Overview

We illustrate the problem of monitoring a parallel program using existing techniques.
In doing so, we overview the related approaches, some of the existing
tools, and their shortcomings. We discuss their assumptions, advantages, limitations,
and suitability when tackling various parallel programs ([scenarios](scenarios/)) In particular, we use manually written monitors using [AspectJ](https://www.eclipse.org/aspectj/),
[Java-MOP](http://fsl.cs.illinois.edu/index.php/JavaMOP4), [MarQ](https://github.com/selig/qea), and [Larva](https://github.com/ccol002/larva-rv-tool).

[RVPredict](http://fsl.cs.illinois.edu/rvpredict/) can be optionally used to check for data-race.

We explore the situations where:

1. a linear trace does not represent the underlying program execution;
2. a linear trace hides some implicit assumptions which affect RV; and
3. it is insufficient to use a linear trace for monitoring multithreaded programs.

By analyzing the interplay between specification formalisms and concurrent executions
of programs, we explore the situations in which it is reliable to use the existing tools and approaches
as well as the situations where we believe more work is needed.

**After setting up, to start the tutorial go to [scenarios](scenarios/).**
