---
title: Producer-Consumer (Unsynchronized)
---

## Unsynchronized Producer-Consumer

### Overview
Producer-Consumer consists of threads that push items to a queue (called [*producers*](src/Producer.java)), and  threads that remove items from the
queue for processing (called [*consumers*](src/Consumer.java)).
To monitor the program we need to keep track of produces and consumes.

The program class and arguments are as follows:

```
ProducerConsumerService <ops> <producers> <consumers>
```

1. `ops`: number of operations to perform, i.e. the number of produces placed on the queue (this will be balanced among producers).
2. `producers`: number of producers.
3. `consumers`: number of consumers.

The default [driver](driver) performs 8 operations using 1 producer and 2 consumers:

```
ProducerConsumerService 8 1 2
```

### Executing the Base Program

To execute the program, compile first using:

```
make refcompile
```

Then execute (this may deadlock):

```
make refrun
```

To specify your own driver, either edit [driver](driver), or simply provide it as a variable:

```
make refrun DRIVER="ProducerConsumerService 16 2 4"
```

When done executing do not forget to clean:

```
make clean
```

### Variant 2

This scenario considers the **second** variant of the program, where locks are absent. Produce and consume occur concurrently on the shared queue.

For simplicity, we have included all the logic in the shared queue ([SynchQueue](src/SyncQueue.java)).
We note that for this implementation we use a spin-lock that keeps checking until the queue is not empty.
The relevant parts are shown below:

```java
public void produce(String str) {
  q.add(str);
}
public String consume() {
  while(q.isEmpty()) {}
  return q.poll();
}
```

You can check that this variant allows a deadlock as we do not have a guarantee that the queue will have an item before calling `q.poll()`, which could cause it to deadlock.
We can test it by executing multiple runs:

```
make sample N=10 TOOL=ref
```

We should observe something like:

```
T:6 F:0 Deadlock:4 U:0
```

We point out that in this case, all terminating runs will be associated with the verdict *true* as we are not monitoring, and a monitor will not print *"Failed!"*, and exit.

### Specification

To illustrate another Java-MOP logic plugin, we express the property as a Context-Free Grammar (CFG) so that is easier to parse ([prodcon.mop](../specs/producer-consumer/prodcon.mop)):

```
S -> S produce S consume | epsilon
```

We express it with [MarQ](../specs/producer-consumer/MarQProdCon.aj) and [Larva](../specs/producer-consumer/prodcon.lrv) as an automaton with a counter that counts produce events.

To capture the events with AspectJ, we need to capture the exact operations on the queue inside the calls to either `SynchQueue.produce` or `SynchQueue.consume`.
To do so, we use pointcut `cflow` to ensure that the calls to the queue are made from calls from the two functions.
The pointcuts are written as (produce and consume, respectively):

```java
call(* Queue.add(*)) && cflow(execution(* SynchQueue.produce(*)))
call(* Queue.poll()) && cflow(execution(* SynchQueue.consume()))
```

For simplicity, in the case of Larva, we use the `Queue.add` and `Queue.poll` calls only.

Two versions of each specification is then generated: in the first version, we capture the events before the call to the functions, while in the second version, we capture the call to events after the call to the functions.
The second version is suffixed by 2, for example:  [prodcon2.mop](../specs/producer-consumer/prodcon.mop).

### Monitoring

To monitor the program simply execute any of the following (depending on the tool):

```
make sample N=10  TOOL=jmop   SPEC=../specs/producer-consumer/prodcon.mop
make sample N=10  TOOL=jmop   SPEC=../specs/producer-consumer/prodcon2.mop
make sample N=10  TOOL=marq   SPEC=../specs/producer-consumer/MarQProdCon.aj
make sample N=10  TOOL=marq   SPEC=../specs/producer-consumer/MarQProdCon2.aj
make sample N=10  TOOL=larva  SPEC=../specs/producer-consumer/prodcon.lrv
make sample N=10  TOOL=larva  SPEC=../specs/producer-consumer/prodcon2.lrv
```


In this case we are using *global monitoring*, but the execution of the program has  **multiple concurrent regions**, as locks are missing.

Notice how in this case, tools may report unreliable verdicts, they either report *true* for some runs, and *false* for others, or simply deadlock with the program.

You can also change the program arguments by passing the `DRIVER` variable.
For example to execute 8 operations using 1 producer and 1 consumer:

```
make sample N=10  TOOL=jmop \
    SPEC=../specs/producer-consumer/prodcon.mop \
    DRIVER="ProducerConsumerService 8 1 1"
```

### Checking Data Race
> RVPredict is **not provided with this tutorial**. This part relies on you having RVPredict already installed, for details on installation check [its webpage](https://runtimeverification    .com/predict/)

It might be interesting to check to confirm that the program is indeed data race free (DRF). **This requires RVPredict**.

First compile the original program using:

```
make refcompile
```

Then execute RVPredict with:

```
make predict
```

We will see plenty of data races resulting from the absence of locks.
We can observe something similar to:

```
rv-predict -cp /home/user/rv-multi/tools/aspectj/lib/aspectjrt.jar:original/ ProducerConsumerService 8 1 2

----------------Instrumented execution to record the trace-----------------
[RV-Predict] Log directory: /tmp/rv-predict3319580120350348838
[RV-Predict] Finished retransforming preloaded classes.
main Complete in 30
Data race on field java.util.LinkedList.$state:
    Write in thread 14
      > at SynchQueue.consume(SynchQueue.java:17)
        at Consumer.run(Consumer.java:13)
    Thread 14 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)

    Read in thread 13
      > at SynchQueue.consume(SynchQueue.java:17)
        at Consumer.run(Consumer.java:13)
    Thread 13 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)


Data race on field java.util.LinkedList.$state:
    Read in thread 13
      > at SynchQueue.consume(SynchQueue.java:16)
        at Consumer.run(Consumer.java:13)
    Thread 13 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)

    Write in thread 14
      > at SynchQueue.consume(SynchQueue.java:17)
        at Consumer.run(Consumer.java:13)
    Thread 14 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)


Data race on field java.util.LinkedList.$state:
    Read in thread 13
      > at SynchQueue.consume(SynchQueue.java:17)
        at Consumer.run(Consumer.java:13)
    Thread 13 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)

    Write in thread 15
      > at SynchQueue.produce(SynchQueue.java:13)
        at Producer.run(Producer.java:19)
    Thread 15 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)


Data race on field java.util.LinkedList.$state:
    Read in thread 13
      > at SynchQueue.consume(SynchQueue.java:16)
        at Consumer.run(Consumer.java:13)
    Thread 13 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)

    Write in thread 15
      > at SynchQueue.produce(SynchQueue.java:13)
        at Producer.run(Producer.java:19)
    Thread 15 created by thread 1
        at java.util.concurrent.ThreadPoolExecutor.addWorker(Unknown Source)

```

Make sure to remove the compiled program when done using:

```
make clean
```
