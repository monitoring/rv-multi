import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.LinkedList;

public class SynchQueue {
	private LinkedList<String> q;

	public SynchQueue(int capacity) {
		this.q = new LinkedList<String>();
	}

	public void produce(String str) {
		q.add(str);
	}
	public String consume() {
		while(q.isEmpty()) {}
    return q.poll();
	}
}
