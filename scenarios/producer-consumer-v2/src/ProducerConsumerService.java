import java.util.Random;
public class ProducerConsumerService {


	public static void main(String[] args) throws InterruptedException {

		Integer capacity 	= Integer.valueOf(args[0]);
		Integer nProds		= Integer.valueOf(args[1]);
		Integer nCons 		= Integer.valueOf(args[2]);

		Integer[] capacities = Harness.balance(capacity - nCons, nProds);
		Integer[] notifies   = Harness.balance(nCons, nProds);


		SynchQueue queue = new SynchQueue(capacity);
		Runnable[] runs = new Runnable[nProds+nCons];

		int i,j;
		for(i = 0; i < nProds; i++)
            runs[i]   = new Producer(capacities[i], notifies[i], queue);
		for(j = 0; j < nCons; j++)
            runs[i+j] = new Consumer(queue);

        Harness bench = new Harness(nCons + nProds);
        bench.start(runs);
        bench.end();


	}

}
