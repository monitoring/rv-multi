
public class Consumer implements Runnable {

	private SynchQueue queue;

	public Consumer(SynchQueue q) {
		this.queue = q;
	}

	@Override
	public void run() {
		while(true) {
				String msg = queue.consume();
				if(msg  == null) continue;
				if(msg.equals("exit"))
					return;
		}
	}
}
