
public class Producer implements Runnable {

	private SynchQueue queue;
	private int cons = 0;
	private int capacity = 0;

	public Producer(int cap, int cons, SynchQueue q) {
		this.queue = q;
		this.cons = cons;
		this.capacity = cap;
	}

	@Override
	public void run() {
		// produce messages
		for (int i = 0; i < capacity; i++) {
			String msg = "" + i;
            queue.produce(msg);
		}
		// adding exit message
		String msg = "exit";
		for(int i = 0; i < cons; i++)
				queue.produce(msg);
	}
}
