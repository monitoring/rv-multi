package qea;

import java.util.Iterator;

//Imports for QEA
import static qea.structure.impl.other.Quantification.FORALL;
import qea.structure.intf.QEA;
import qea.creation.QEABuilder;
import qea.monitoring.impl.*;
import qea.monitoring.intf.*;
import qea.structure.impl.other.Verdict;
import qea.properties.papers.HasNextQEA;

public aspect SliceThread {

        // Declaring the events
        private final int HASNEXT_TRUE = 1;
        private final int HASNEXT_FALSE = 2;
        private final int NEXT = 3;

        //The monitor
        Monitor monitor;
        // Required if multithreaded as monitor not thread-safe
        private Object LOCK = new Object();


        pointcut hasNext() : call(* java.util.Iterator+.hasNext());
        pointcut next() :  call(* java.util.Iterator+.next());

        after() returning(boolean b): hasNext() {
                synchronized(LOCK){
                        Long i = Thread.currentThread().getId();
                        if(b){ check(monitor.step(HASNEXT_TRUE,i)); }
                        else { check(monitor.step(HASNEXT_FALSE,i)); }
                }
        }

        before() : next() {
                synchronized(LOCK){
                        Long i = Thread.currentThread().getId();
                        check(monitor.step(NEXT,i));
                }
        }

        private static void check(Verdict verdict){
                if(verdict==Verdict.FAILURE){
                    System.err.println("Failed!");
                    System.exit(1);
                }
        }

        public SliceThread(){
                //QEA qea = new qea.properties.papers.HasNextQEA();

                QEABuilder b = new QEABuilder("HasNext");

                int i = -1;
                b.addQuantification(FORALL,i);

                int HASNEXT_TRUE = 1;
                int HASNEXT_FALSE = 2;
                int NEXT = 3;

                b.addTransition(1,HASNEXT_TRUE,i,2);
                b.addTransition(2,HASNEXT_TRUE,i,2);
                b.addTransition(2,NEXT,i,1);

                //Need to add this.
                b.addTransition(1,HASNEXT_FALSE,i,3);

                b.addTransition(2,HASNEXT_FALSE,i,3);
                b.addTransition(3,HASNEXT_FALSE,i,3);

                b.addFinalStates(1,2,3);

                QEA qea = b.make();

                monitor = MonitorFactory.create(qea);
        }
}
