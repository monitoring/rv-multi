package qea;

import java.util.Iterator;

//Imports for QEA
import static qea.structure.impl.other.Quantification.FORALL;
import qea.structure.intf.QEA;
import qea.creation.QEABuilder;
import qea.monitoring.impl.*;
import qea.monitoring.intf.*;
import qea.structure.impl.other.Verdict;
import qea.properties.papers.HasNextQEA;

public aspect SliceBoth {

        // Declaring the events
        private final int HASNEXT_TRUE = 1;
        private final int HASNEXT_FALSE = 2;
        private final int NEXT = 3;

        //The monitor
        Monitor monitor;
        // Required if multithreaded as monitor not thread-safe
        private Object LOCK = new Object();


        pointcut hasNext(Iterator i) : call(* java.util.Iterator+.hasNext())
            && target(i);
        pointcut next(Iterator i) :  call(* java.util.Iterator+.next())
            && target(i);

        after(Iterator i) returning(boolean b): hasNext(i) {
                synchronized(LOCK){
                        Long t = Thread.currentThread().getId();
                        if(b){ check(monitor.step(HASNEXT_TRUE,t,i)); }
                        else { check(monitor.step(HASNEXT_FALSE,t,i)); }
                }
        }

        before(Iterator i) : next(i) {
                synchronized(LOCK){
                        Long t = Thread.currentThread().getId();
                        check(monitor.step(NEXT,t,i));
                }
        }

        private static void check(Verdict verdict){
                if(verdict==Verdict.FAILURE){
                    System.err.println("Failed!");
                    System.exit(1);
                }
        }

        public SliceBoth(){
                QEABuilder b = new QEABuilder("HasNext");

                int t = -1;
                int i = -2;
                b.addQuantification(FORALL,t);
                b.addQuantification(FORALL,i);

                int HASNEXT_TRUE = 1;
                int HASNEXT_FALSE = 2;
                int NEXT = 3;

                b.addTransition(1,HASNEXT_TRUE,new int[]{t,i},2);
                b.addTransition(2,HASNEXT_TRUE,new int[]{t,i},2);
                b.addTransition(2,NEXT,new int[]{t,i},1);

                //Need to add this.
                b.addTransition(1,HASNEXT_FALSE,new int[]{t,i},3);

                b.addTransition(2,HASNEXT_FALSE, new int[]{t,i},3);
                b.addTransition(3,HASNEXT_FALSE, new int[]{t,i},3);

                b.addFinalStates(1,2,3);

                QEA qea = b.make();

                monitor = MonitorFactory.create(qea);
        }
}
