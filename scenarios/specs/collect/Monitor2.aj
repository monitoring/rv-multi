public aspect Monitor2{
  private Object LOCK = new Object();
  after() : call(void DoG.g())
			{  synchronized(LOCK) {System.err.println("g_trace");} }

  after() : call(void DoF.f())
			{  synchronized(LOCK) {System.err.println("f_trace");} }
}
