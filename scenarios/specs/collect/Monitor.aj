public aspect Monitor{
  private Object LOCK = new Object();
  before() : call(void DoG.g())
			{  synchronized(LOCK) {System.err.println("g_trace");} }

  before() : call(void DoF.f())
			{  synchronized(LOCK) {System.err.println("f_trace");} }
}
