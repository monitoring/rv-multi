import qea.structure.intf.QEA;
import qea.creation.QEABuilder;
import qea.monitoring.impl.*;
import qea.monitoring.intf.*;
import qea.structure.impl.other.Verdict;
import qea.structure.intf.*;

import java.util.*;

public aspect MarQProdCon2 {

  //Events
  private final int PRODUCE = 1;
  private final int CONSUME = 2;

  //Produce Counter
  private int counter = 0;

  //Monitor + Lock
  Monitor monitor;
  private Object LOCK = new Object();


  after() : //Handle Event: Produce
    call(* Queue.add(*))
    && cflow(execution(* SynchQueue.produce(*)))
  {
    synchronized(LOCK){
      check(monitor.step(PRODUCE, counter));
      counter++;
    }
  }

  after() : //Handle Event: Consume
    call(* Queue.poll())
    && cflow(execution(* SynchQueue.consume()))
  {
    synchronized(LOCK){
      check(monitor.step(CONSUME, counter));
      counter--;
     }
  }

  private void check(Verdict verdict){
    if(verdict==Verdict.FAILURE){
      System.err.println("Failed!");
      System.exit(1);
    }
  }

  //Create QEA Specification
  public void init(){
    QEABuilder b = new QEABuilder("ProdCon");
    int ticket = 1;

    b.addTransition(1, PRODUCE, new int[] {ticket},
      Assignment.increment(ticket),        1);

    b.addTransition(1, CONSUME, new int[] {ticket},
      Guard.varIsGreaterThanVal(ticket, 0),
      Assignment.decrement(ticket),        1);

    b.addTransition(1, CONSUME, new int[] {ticket},
      Guard.varIsEqualToIntVal(ticket, 0), 2);

    b.addFinalStates(1);

    monitor = MonitorFactory.create(b.make());
  }

  public MarQProdCon2(){ init(); }
}
