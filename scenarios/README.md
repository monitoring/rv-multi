---
title: Tutorial Scenarios
---

This folder contains various scenarios for monitoring multithreaded programs.
It includes both code and [specifications](specs/).


## Usage of Tools

All tools are streamlined into a single process that utilizes `Make` targets.

Each tool is given a short name, and associated 3 make targets:

* `compile`: compiles the source code with the monitors, users must specify the specification path using `SPEC=/path/to/spec`.
* `run` : executes the program alongside the monitors. By default the program arguments are found in a file called `driver`, users can specify their own arguments by specifying `DRIVER="arg0 arg1 arg2"`.
* `clean` : to remove the working directory

For example for Java-MOP, the short name is `jmop`, the following `make` targets are available: `jmopcompile`, `jmoprun`, and `jmopclean`.

The available tools for monitoring are: `jmop` (Java-MOP), `marq`, `larva`, and `aj` for a manually written AspectJ aspect.

Furthermore `ref` (`refcompile`, `refrun`, and `refclean`) is supported to execute the program without monitoring.


## Scenarios

*The scenarios are presented in the order of the tutorial, as such it is best to start with the first one.*

### Per-thread List Processing (with Iterators)

This simple program uses two threads to process a shared list of integers concurrently.

Each thread creates an iterator on the shared list, the first finds the
minimum, while the second finds the maximum.

The folder [process](process/) contains the scenario.

The property checked ([specs/process](specs/process)) is that of proper iterator usage. Where a call to `next()` should only happen after calling `hasNext()`.

### Producer-Consumer

Producer-Consumer consists of threads that push items to a queue (called *producers*), and  threads that remove items from the
queue for processing (called *consumers*).
To monitor the program we need to keep track of produces and consumes.
We include the two variants for Producer-Consumer.

* [Variant 1 (v1)](producer-consumer-v1/): contains the properly synchronized code.
* [Variant 2 (v2)](producer-consumer-v2/): contains the same code without any locks.

Both variants share the same specification ([specs/producer-consumer](specs/producer-consumer)).
The specification roughly states that all produces must eventually be matched with a consume, and no consume should occur when there is nothing on the queue.

### Trace Collection

The *Trace Collection* scenario is designed to illustrate the problem of tools attempting to capture a trace of a program where there is concurrency.
Typically, the code designed to feed the information to the monitor is inserted after the matched code for the event.
However, the matching and the execution of the additional code are not atomic, which could lead to race conditions.

In this scenario, we create two threads such that each calls a unique function (f and g, respectively) an equal number of times.
Each function consists of a single print statement (to stdout) indicating the function name. We create a simple monitor that prints (to stderr) the same function name while appending “\_trace”.
Then, we verify that the traces are identical, that is the prints from within the functions follow the same order as those in the monitor.

The folder [collect](collect/) contains the scenario.

The specification found in [specs/collect](specs/collect) specifies the monitors designed to print the events received.
No checking is performed.
