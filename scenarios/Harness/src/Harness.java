import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
public class Harness {
	private ExecutorService pool;
	private Long start;

	public Harness(int threads) {
		pool = Executors.newFixedThreadPool(threads);
	}
	public void start(Runnable[] runnables) {
			start(runnables, System.currentTimeMillis());
	}

	public void start(Runnable[] runnables, Long seed) {
		shuffle(runnables, seed);
		start = System.currentTimeMillis();
		for(Runnable r : runnables)
		    pool.submit(r);
	}

	public void end() {
		pool.shutdown();
		try {
            pool.awaitTermination(100, TimeUnit.HOURS);
        } catch(Exception e) {

        }
		log("Complete in " + (System.currentTimeMillis() - start));
	}

	private void shuffle(Runnable[] runnables, Long seed) {
	    Random r = new Random();
	    int n = runnables.length;
	    Runnable t;

	    for(int i = 0; i < n; i++) {
	        int idx = r.nextInt(n);
	        t = runnables[idx];
	        runnables[idx]  = runnables[i];
	        runnables[i]    = t;
        }

    }

	public static void log(String msg) {
		System.out.println(Thread.currentThread().getName() + " " + msg);
	}
    public static Integer[] balance(int number, int takers) {
        int left  = number;
        int max   = 100;
        Random r  = new Random();
        Integer[] load = new Integer[takers];

        int i;
        for(i = 0; i < takers - 1; i++) {
            int percent = r.nextInt(max);
            max 	-= percent;
            load[i] = (int) Math.floor((double) number * ( (double) percent / 100.0));
            left 	-= load[i];
        }
        load[i] =  left;
        return load;
    }
}
