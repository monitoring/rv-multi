# Benchmark Harness

The [benchmark Harness class](src/Harness.java) provides utility methods to run the examples:

* Start/Stop timing
* Scheduler pool setup to be fixed (per number of threads provided)
* Randomized starts for Runnables
* Utility functions for randomizing load