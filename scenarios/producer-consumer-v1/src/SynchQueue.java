import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;


public class SynchQueue {
	private LinkedList<String> q;
	//private Semaphore lock = new Semaphore(1);
	private ReentrantLock lock = new ReentrantLock();

	public SynchQueue(int capacity) {
		this.q = new LinkedList<String>();
	}

/*
	private boolean isEmpty() {
		return q.isEmpty();
	}
*/
	public void produce(String str) {
		//try {
			//lock.acquire();
			lock.lock();
			q.add(str);
			System.out.println("Produce");
			//lock.release();
			lock.unlock();
		//} catch (InterruptedException ex) {

		//}
	}

	public String consume() {
			String str;
			//try {
			while (true) {
				//lock.acquire();
				lock.lock();
				if(!q.isEmpty()) {
					str = q.poll();
					System.out.println("Consume " + str);
					//lock.release();
					lock.unlock();
					return str;
				}
				else
					//lock.release();
					lock.unlock();
			}
		//} catch (InterruptedException ex) {
		//	return null;
		//}
	}
}
