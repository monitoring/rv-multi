---
title: Producer-Consumer (Synchronized)
---

## Producer-Consumer with Reentrant Locks

### Overview
Producer-Consumer consists of threads that push items to a queue (called [*producers*](src/Producer.java)), and  threads that remove items from the
queue for processing (called [*consumers*](src/Consumer.java)).
To monitor the program we need to keep track of produces and consumes.

The program class and arguments are as follows:

```
ProducerConsumerService <ops> <producers> <consumers>
```

1. `ops`: number of operations to perform, i.e. the number of produces placed on the queue (this will be balanced among producers).
2. `producers`: number of producers.
3. `consumers`: number of consumers.

The default [driver](driver) performs 8 operations using 1 producer and 2 consumers:

```
ProducerConsumerService 8 1 2
```

### Executing the Base Program

To execute the program, compile first using:

```
make refcompile
```

Then execute:

```
make refrun
```

To specify your own driver, either edit [driver](driver), or simply provide it as a variable:

```
make refrun DRIVER="ProducerConsumerService 16 2 4"
```

When done executing do not forget to clean:

```
make clean
```

### Variant 1

This scenario considers the **first** variant of the program, where it is properly synchronized with locks. Produce and consume occur atomically on the shared queue.

For simplicity, we have included all the logic in the shared queue ([SynchQueue](src/SyncQueue.java)).
We note that for this implementation we use a spin-lock that keeps checking until the queue is not empty.
The relevant parts are shown below:

```java
public void produce(String str) {
    lock.lock();
    q.add(str);
    System.out.println("Produce");
    lock.unlock();
}
public String consume() {
    String str;
    while (true) {
      lock.lock();
      if(!q.isEmpty()) {
        str = q.poll();
        System.out.println("Consume " + str);
        lock.unlock();
        return str;
      }
      else
        lock.unlock();
    }
}
```

### Specification

To illustrate another Java-MOP logic plugin, we express the property as a Context-Free Grammar (CFG) so that is easier to parse ([prodcon.mop](../specs/producer-consumer/prodcon.mop)):

```
S -> S produce S consume | epsilon
```

We express it with [MarQ](../specs/producer-consumer/MarQProdCon.aj) and [Larva](../specs/producer-consumer/prodcon.lrv) as an automaton with a counter that counts produce events.

To capture the events with AspectJ, we need to capture the exact operations on the queue inside the calls to either `SynchQueue.produce` or `SynchQueue.consume`.
To do so, we use pointcut `cflow` to ensure that the calls to the queue are made from calls from the two functions.
The pointcuts are written as (produce and consume, respectively):

```java
call(* Queue.add(*)) && cflow(execution(* SynchQueue.produce(*)))
call(* Queue.poll()) && cflow(execution(* SynchQueue.consume()))
```

For simplicity, in the case of Larva, we use the `Queue.add` and `Queue.poll` calls only.

Two versions of each specification is then generated: in the first version, we capture the events before the call to the functions, while in the second version, we capture the call to events after the call to the functions.
The second version is suffixed by 2, for example:  [prodcon2.mop](../specs/producer-consumer/prodcon.mop).

### Monitoring

To monitor the program simply execute any of the following (depending on the tool):

```
make sample N=10  TOOL=jmop   SPEC=../specs/producer-consumer/prodcon.mop
make sample N=10  TOOL=jmop   SPEC=../specs/producer-consumer/prodcon2.mop
make sample N=10  TOOL=marq   SPEC=../specs/producer-consumer/MarQProdCon.aj
make sample N=10  TOOL=marq   SPEC=../specs/producer-consumer/MarQProdCon2.aj
make sample N=10  TOOL=larva  SPEC=../specs/producer-consumer/prodcon.lrv
make sample N=10  TOOL=larva  SPEC=../specs/producer-consumer/prodcon2.lrv
```

Notice how in this case, they all report *true* for all runs.

In this case we are using *global monitoring*, but the execution of the program has **no concurrent regions**, all concurrency is removed by the locks on the queue.

You can also change the program arguments by passing the `DRIVER` variable.
For example to execute 8 operations using 1 producer and 1 consumer:

```
make sample N=10  TOOL=jmop \
    SPEC=../specs/producer-consumer/prodcon.mop \
    DRIVER="ProducerConsumerService 8 1 1"
```

### Checking Data Race
> RVPredict is **not provided with this tutorial**. This part relies on you having RVPredict already installed, for details on installation check [its webpage](https://runtimeverification.com/predict/)

It might be interesting to check to confirm that the program is indeed data race free (DRF). **This requires RVPredict**.

First compile the original program using:

```
make refcompile
```

Then execute RVPredict with:

```
make predict
```

This will show *"No races found"* at the end of the program, or information on races if any occured.

In this case we can observe something similar to:

```
rv-predict -cp /home/user/rv-multi/tools/aspectj/lib/aspectjrt.jar:original/ ProducerConsumerService 8 1 2

----------------Instrumented execution to record the trace-----------------
[RV-Predict] Log directory: /tmp/rv-predict1198237485892124527
[RV-Predict] Finished retransforming preloaded classes.
Produce
Consume 0
Produce
Consume 1
Produce
Consume 2
Produce
Consume 3
Produce
Consume 4
Produce
Produce
Produce
Consume 5
Consume exit
Consume exit
main Complete in 34
No races found.
```

Make sure to remove the compiled program when done using:

```
make clean
```
