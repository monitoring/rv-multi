AJCP?=${AJ}/lib/aspectjrt.jar
RVCP?=${AJ}/lib/aspectjrt.jar:${JMOP}/lib:${RVMON}/../lib/rv-monitor-rt.jar

# Defaults
HARN?=../Harness/src
SPEC?=spec.mop
DRIVER?=${shell cat driver}

# Where are we executing RV-Predict
RVP_CP?=original/

# Parameters
N?=10
TOOL?=ref

.PHONY: default scenario pclean clean refclean refcompile refrun predict marqcompile marqrun marqclean larvacompile larvarun larvaclean ajcompile ajrun ajclean jmopcompile jmoprun jmopclean

default: scenario

# Default program
refclean:
	@rm -rf original 2>/dev/null

refcompile: refclean
	mkdir original
	javac -d original ${HARN}/*
	javac -cp original/ -d original src/*

refrun:
	java -cp original/ ${DRIVER}

# RV Predict Goodies
predict:
	rv-predict -cp ${AJCP}:${RVP_CP} ${DRIVER}

# MarQ
marqcompile: marqclean
	@mkdir -p marq
	ajc -showWeaveInfo -cp ${AJCP}:${MARQ} \
		-1.8 -sourceroots src:${HARN} -d marq \
		${SPEC}

marqrun:
	java -cp ${AJCP}:${MARQ}:marq/ ${DRIVER}

marqclean:
	make -s pclean WDIR=marq

# LARVA
larvacompile: larvaclean
	@mkdir -p larva
	java -cp ${AJCP}:${LARVA} compiler.Compiler ${SPEC} -o larva
	ajc -showWeaveInfo -cp ${AJCP}:${LARVA} \
		-1.8 -sourceroots larva:src:${HARN} -d larva

larvarun:
	java -cp ${AJCP}:${LARVA}:larva ${DRIVER}

larvaclean:
	make -s pclean WDIR=larva

# Arbitrary AspectJ
ajcompile: ajclean
	@mkdir aj
	ajc -showWeaveInfo -cp ${AJCP} \
 		-1.8 -sourceroots src:${HARN} -d aj \
		${SPEC}
ajrun:
	java -cp ${AJCP}:aj/ ${DRIVER}

ajclean:
	make -s pclean WDIR=aj

# Java-MOP
jmopcompile: jmopclean
	@mkdir jmop
	javamop -d jmop ${SPEC}
	(cd jmop && rv-monitor *.rvm) #Not needed with some versions of JavaMOP/RV-Monitor
	ajc -showWeaveInfo -cp ${RVCP} \
 		-1.8 -sourceroots jmop:src:${HARN} -d jmop

jmoprun:
	java -cp ${RVCP}:jmop/ ${DRIVER}

jmopclean:
	make -s pclean WDIR=jmop

# Remove a working folder
pclean:
	-@rm -rf ${WDIR} 2>/dev/null

# Clean All
clean: refclean jmopclean marqclean ajclean larvaclean
	-rm -rf *.log *.1k *.10k 2>/dev/null
	@(test -f clean.sh && ./clean.sh) || true


# Sample result of monitoring across multiple executions
sample:
	@echo "## Sampling N=$N TOOL=${TOOL} SPEC=${SPEC} ARGS=${DRIVER}" | tee ${TOOL}-meta.log
	@echo -e "\n\n## Compiling (RAW)" | tee -a ${TOOL}-meta.log
	make -si ${TOOL}compile 2>&1 | tee -a ${TOOL}-meta.log
	@echo -e "\n\n## Events" | tee -a ${TOOL}-meta.log
	@grep "method-call" ${TOOL}-meta.log | tee -a ${TOOL}-meta.log
	@echo -e "\n\n## Executing" | tee -a ${TOOL}-meta.log
	../scripts/sample.sh $N ${TOOL}run "${DRIVER}" | tee -a ${TOOL}-meta.log
	make -si ${TOOL}clean

# Default
scenario:
	${info See documentation for details}
