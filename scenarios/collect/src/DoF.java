
public class DoF implements Runnable {

	private int times = 0;

	public DoF(int n) {
		this.times = n;
	}

	public void f() {
		System.out.println("f");
	}

	@Override
	public void run() {
		for(int i = 0; i < times; i++)
				f();
	}
}
