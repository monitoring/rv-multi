
public class DoG implements Runnable {

	private int times = 0;

	public DoG(int n) {
		this.times = n;
	}

	public void g() {
		System.out.println("g");
	}

	@Override
	public void run() {
		for(int i = 0; i < times; i++)
				g();
	}
}
