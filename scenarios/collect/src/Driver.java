import java.util.Random;
public class Driver {


	public static void main(String[] args) throws InterruptedException {

		Integer capacity 	= Integer.valueOf(args[0]);
		Integer nF		= Integer.valueOf(args[1]);
		Integer nG 		= Integer.valueOf(args[2]);



		Runnable[] runs = new Runnable[nF+nG];

		int i,j;
		for(i = 0; i < nF; i++)
            runs[i]   = new DoF(capacity);
		for(j = 0; j < nG; j++)
            runs[i+j] = new DoG(capacity);


    Harness bench = new Harness(nF + nG);
    bench.start(runs);
    bench.end();
	}

}
