#!/bin/bash
TARGET=$1
DRIVER=$2
EX=${3:-rm}
make -si ${TARGET} DRIVER="$DRIVER" 1>log1${TARGET}.txt 2>log2${TARGET}.txt
len=`wc -l log1${TARGET}.txt | cut -d' ' -f 1`
totake=$(( len - 1 ))
head -n $totake log1${TARGET}.txt > orig${TARGET}.txt
grep "_trace" log2${TARGET}.txt | sed "s,_trace,,g" > trace${TARGET}.txt
${EX} log1${TARGET}.txt log2${TARGET}.txt
diff -q trace${TARGET}.txt orig${TARGET}.txt
rm trace${TARGET}.txt orig${TARGET}.txt
