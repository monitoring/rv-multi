---
title: Trace Collection
---

## Trace Collection and Instrumentation Interference

### Overview

The *Trace Collection* scenario is designed to illustrate the problem of tools attempting to capture a trace of a program where there is concurrency.
Typically, the code designed to feed the information to the monitor is inserted after the matched code for the event.
However, the matching and the execution of the additional code are not atomic, which could lead to race conditions.

In this scenario, we create two threads ([DoF](src/DoF.java), and [DoG](src/DoG.java)) such that each calls a unique function (`f` and `g`, respectively) an equal number of times.
Each function consists of a single print statement (to stdout) indicating the function name.

We create a simple monitor that prints (to stderr) the same function name while appending “\_trace”.
*The monitor performs no checking.*

Then, we verify that the traces are identical, that is the prints from within the functions follow the same order as those in the monitor.

The program class and arguments are as follows:

```
Driver <calls> <nf> <ng>
```

1. `calls`: number of calls to each function to execute.
2. `nf`: number of threads that can call `f`.
3. `ng`: number of threads that can call `g`.

The default [driver](driver) uses the following arguments: 8 1 1.

```
Driver 8 1 1
```

While we restrict this scenario to 1 thread for each `f` and `g` for simplicity, the general program can be used to experiment with various other tweaks.

### Executing the Base Program

To execute the program, compile first using:

```
make refcompile
```

Then execute:

```
make refrun
```

To specify your own driver, either edit [driver](driver), or simply provide it as a variable:

```
make refrun DRIVER="Driver 10 1 1"
```

When done executing do not forget to clean:

```
make clean
```

### Writing the Specification


The events can be captured with AspectJ using the following pointcuts as they are function calls:

```java
call(void DoG.g())
call(void DoF.f())
```

We build basic monitors ([AspectJ](../specs/collect/Monitor.aj), [Java-MOP](../specs/collect/spec.mop), and [Larva](../specs/collect/spec.lrv)) that only print to `stderr` the function named appended with *\_trace*.

We focused on using the synchronized monitors that each platform provides, to try to capture the trace their global monitors will capture. For AspectJ we added (possibly redundantly, as printing still may induce locking) a lock to simulate its usage with MarQ.

We also provided an [unsynchronized specification for Java-MOP](../specs/collect/specunsync.mop) for reference.

All these specification capture events before the call.
They can be suffixed with 2 for similar specifications but that capture events after the call ([AspectJ](../specs/collect/Monitor2.aj), [Java-MOP](../specs/collect/spec2.mop), [Larva](../specs/collect/spec2.lrv), and [Java-MOP Unsync](../specs/collect/specunsync2.mop)).

### Comparing A Single Execution

If you wish to compare a single execution use the `keep` target, execute:

```
make keep TOOL=aj  SPEC=../specs/collect/Monitor2.aj
```

This executes the run, then prints the contents of `stdout` followed by `stderr`.

For example we see this output:
```
f
g
f
g
f
g
f
g
f
g
g
g
g
f
f
f
main Complete in 15
f_trace
g_trace
f_trace
g_trace
f_trace
g_trace
f_trace
g_trace
g_trace
g_trace
g_trace
f_trace
g_trace
f_trace
f_trace
f_trace
```

You can see that the two traces differ by looking at the 5th event starting from the bottom which is `g` in `stdout` but `f` in `stderr`.

This will create two log files based on the tool used, for `aj` the files are `log1ajrun.txt` and `log2ajrun.txt`, the first contains the `stdout`, while the second contains `stderr`.

Make sure to clean:
```
make clean
```

### Comparing Traces

To compare multiple traces automatically (by processing and executing diff) simply execute any of the following:

(Before)

```
make compare N=10  TOOL=aj    SPEC=../specs/collect/Monitor.aj
make compare N=10  TOOL=jmop  SPEC=../specs/collect/spec.mop
make compare N=10  TOOL=larva SPEC=../specs/collect/spec.lrv
```

(After)

```
make compare N=10  TOOL=aj    SPEC=../specs/collect/Monitor2.aj
make compare N=10  TOOL=jmop  SPEC=../specs/collect/spec2.mop
make compare N=10  TOOL=larva SPEC=../specs/collect/spec2.lrv
```
