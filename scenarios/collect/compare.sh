#!/bin/bash
DEFDRIVER=`cat driver`
TARGET=${2:-jmoprun}
DRIVER=${3:-$DEFDRIVER}
rm -rf trace.txt  log.txt orig.txt
s=0
d=0
for i in $(seq 1 $1); do
	echo -n "$i "
	v=`./process.sh $TARGET "$DRIVER"`
	if [[ -z $v ]]; then
		echo "Similar"
		s=$((s + 1))
	else
		echo "Traces Differ"
		d=$((d + 1))
	fi
done
echo "Similar:$s Different:$d"
