import java.util.*;

public class ProcessMin implements Runnable {

	private List<Integer> queue;

	public ProcessMin(List<Integer> q) {
		this.queue = q;
	}

	@Override
	public void run() {
		if(queue == null) return;
		Iterator<Integer> iter = queue.iterator();

		if(!iter.hasNext()) return;
		Integer min = iter.next();

		while(iter.hasNext())
			min = Math.min(min, iter.next());

		System.out.println("Minimum: " + min);
	}
}
