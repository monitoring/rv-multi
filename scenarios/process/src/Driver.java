import java.util.*;
public class Driver {


	public static void main(String[] args) throws InterruptedException {

		Integer items 	= Integer.valueOf(args[0]);

		if(items <= 0) System.exit(1);

		Random r = new Random(System.currentTimeMillis());


		List<Integer> list = new LinkedList<Integer>();
		for(int i = 0; i < items; i++)
			list.add(r.nextInt(items));

		Runnable[] runs = new Runnable[] {
			new ProcessMin(list), new ProcessMax(list)
		};


    Harness bench = new Harness(2);
    bench.start(runs);
    bench.end();
	}

}
