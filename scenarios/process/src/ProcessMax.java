import java.util.*;

public class ProcessMax implements Runnable {

	private List<Integer> queue;

	public ProcessMax(List<Integer> q) {
		this.queue = q;
	}

	@Override
	public void run() {
		if(queue == null) return;
		Iterator<Integer> iter = queue.iterator();

		if(!iter.hasNext()) return;
		Integer max = iter.next();

		while(iter.hasNext())
			max = Math.max(max, iter.next());

		System.out.println("Maximum: " + max);
	}
}
