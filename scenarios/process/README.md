---
title: Per-thread List Processing
---

### Overview

This simple program uses two threads to process a shared list of integers concurrently.

Each thread creates an iterator on the shared list, the first finds the
minimum ([ProcessMin.java](src/ProcessMin.java)), while the second finds the maximum ([ProcessMax.java](src/ProcessMax.java)).

The program argument consists of the size of the list.

### Informal Specification

We are interested in checking the classical **hasNext** property for iterators.
That is, we are interested in checking the following:

> A call to `Iterator.next()` should always be preceded with a call to `Iterator.hasNext()`.

For this particular example, we will not focus on ensuring that `Iterator.hasNext()` also returned `true`, furthermore we consider that iterators will not be shared across threads.

This allows us to perform *per-thread monitoring* where we consider each thread an independent program to monitor.

### Executing the Base Program

To execute the program, compile first using:

```
make refcompile
```

Then execute:

```
make refrun
```

To specify your own driver, either edit [driver](driver), or simply provide it as a variable:

```
make refrun DRIVER="Driver 30"
```

When done executing do not forget to clean:

```
make clean
```

### Writing the Specification

The specification consists of two events: `next` and `hasnext`, these events correspond respectively to calls to functions `Iterator.next()` and `Iterator.hasNext()`.

The events can be captured with AspectJ using the following pointcuts as they are function calls:

```java
call(* Iterator.hasNext())
call(* Iterator.next())
```

To express the specification, we use a finite state machine with three states (`start`, `safe`, and `unsafe`), it can be written as such with the Java-MOP `fsm` logic plugin:
```
 start [
     next    -> unsafe
     hasnext -> safe
 ]
 safe [
     next    -> start
     hasnext -> safe
 ]
 unsafe [
     next    -> unsafe
     hasnext -> unsafe
 ]
```
We see that to reach the `unsafe` state, we must indeed violate the property.

### Developing The Global Monitor (Simplest)

By combining the automaton with the pointcuts, we can generate a simple monitor ([iter.mop](../specs/process/mop/iter.mop)):

```
HasNext() {

       event hasnext before() :
            call(* Iterator.hasNext())
              {}

       event next before() :
             call(* Iterator.next())
             {}

       fsm :
           start [
               next    -> unsafe
               hasnext -> safe
           ]
           safe [
               next    -> start
               hasnext -> safe
           ]
           unsafe [
               next    -> unsafe
               hasnext -> unsafe
           ]

       alias match = unsafe

       @match {
         System.err.println("Failed!");
         System.exit(1);
       }
}
```

We note that this specification applies across **all** threads and iterators, as such it is global, and can fail on a program that has more than one iterator.

We can test this by executing the monitor alongside the program, and look at multiple executions.

Lets first test one execution, compile the spec using **ANY** of the tools.
You can also check the equivalent specs for the other tools: [Marq](../specs/process/marq/Iter.aj) and [Larva](../specs/process/larva/iter.lrv).

```
make jmopcompile    SPEC=../specs/process/mop/iter.mop
make marqcompile    SPEC=../specs/process/marq/Iter.aj
make larvacompile   SPEC=../specs/process/larva/iter.lrv
```

Once compiled you can execute the program with the monitor using:

```
make jmoprun
make marqrun
make larvarun
```

You will notice that all monitors will fail after a few executions, this is not surprising as we have two iterators running each on a separate thread.


If you are interested in a single execution trace, you can for example check the Larva output file under `larva/output_iter.txt` it will list the automata state transition right before reaching the bad state.

In this case, this will **not** be useful, as Larva won't log the last move as the monitor has a `System.exit(1)` and will exit the program before it can be logged.

```
cat  larva/output_iter.txt
```

Here is an example log:

```
[iterhasnext]AUTOMATON::> iterhasnext() STATE::>starting
[iterhasnext]MOVED ON METHODCALL: boolean java.util.Iterator.hasNext() TO STATE::> safe
[iterhasnext]AUTOMATON::> iterhasnext() STATE::>safe
[iterhasnext]MOVED ON METHODCALL: boolean java.util.Iterator.hasNext() TO STATE::> safe
[iterhasnext]AUTOMATON::> iterhasnext() STATE::>safe
[iterhasnext]MOVED ON METHODCALL: Object java.util.Iterator.next() TO STATE::> starting
[iterhasnext]AUTOMATON::> iterhasnext() STATE::>starting
[iterhasnext]MOVED ON METHODCALL: boolean java.util.Iterator.hasNext() TO STATE::> safe
[iterhasnext]AUTOMATON::> iterhasnext() STATE::>safe
[iterhasnext]MOVED ON METHODCALL: Object java.util.Iterator.next() TO STATE::> starting
[iterhasnext]AUTOMATON::> iterhasnext() STATE::>starting
```


We can see that the last valid state was the `starting` state, and then we must have received a `next` to fail.

You can now check the same specs across multiple runs of the same program.

First cleanup:

```
make clean
```

Then sample *N* runs sing the following:

```
make sample N=10  TOOL=jmop    SPEC=../specs/process/mop/iter.mop
make sample N=10  TOOL=marq    SPEC=../specs/process/marq/Iter.aj
make sample N=10  TOOL=larva   SPEC=../specs/process/larva/iter.lrv
```

The result line should appear like so:

```
T:0 F:10 Deadlock:0 U:0
```

This indicates that in our 10 executions (N) we observed 0 *true verdicts* (T), 10 *false verdicts* (F), 0 *timeouts* (Deadlock).
The *unknown* (U) counter counts the number of executions where the output did not parse properly.



### Monitor per Iterator (Slicing on Iterator)
To improve on this monitor, we need to make sure to specify that this specification applies to each iterator.

This is called *slicing*, it allows the monitor state to depend on specific parameters.

To create a monitor per iterator, all we have to set the parameter of the monitor to be bound to instances of `java.util.Iterator`.

Then when capturing the events, we must identify the parameter in the matching. Our monitor becomes [iterslice.mop](../specs/process/mop/iterslice.mop) ([MarQ](../specs/process/marq/SliceIter.aj), [Larva](../specs/process/larva/iterslice.lrv)).

```
HasNext(Iterator i) {

       event hasnext before(Iterator i) :
             call(* Iterator.hasNext())
             && target(i) {}

       event next before(Iterator i) :
             call(* Iterator.next())
             && target(i) {}

       fsm :
           start [
               next    -> unsafe
               hasnext -> safe
           ]
           safe [
               next     -> start
               hasnext  -> safe
           ]
           unsafe [
               next     -> unsafe
               hasnext  -> unsafe
           ]

       alias match = unsafe

       @match {
         System.err.println("Failed!");
         System.exit(1);
       }
}
```

You can now test the following specification with the tools:

```
make sample N=10  TOOL=jmop    SPEC=../specs/process/mop/iterslice.mop
make sample N=10  TOOL=marq    SPEC=../specs/process/marq/SliceIter.aj
make sample N=10  TOOL=larva   SPEC=../specs/process/larva/iterslice.lrv
```

You should now see that verdicts are consistently *true*.
The result line should be:

```
T:10 F:0 Deadlock:0 U:0
```

This is because each iterator runs in a thread, so there is no concurrency when dealing with the iterators.
This assumption holds only for our program.
Ideally, we need to also slice per thread.

### Per-Thread Monitor (Slicing on Threads)

To perform *per-thread* monitoring, we need to instanciate a monitor for each thread.
This can be seen as simply *slicing* per thread.
We illustrate it in [Java-MOP](../specs/process/mop/threadslice.mop) and [MarQ](../specs/process/marq/SliceThread.aj) (this requires some hacking around to do in Larva, but it is possible).

```
HasNext(Thread i) {

       event hasnext before(Thread i) :
             call(* Iterator.hasNext())
             && thread(i) {}

       event next before(Thread i) :
             call(* Iterator.next())
             && thread(i) {}

       fsm :
           start [
               next     -> unsafe
               hasnext  -> safe
           ]
           safe [
               next     -> start
               hasnext  -> safe
           ]
           unsafe [
               next     -> unsafe
               hasnext  -> unsafe
           ]

       alias match = unsafe

       @match {
         System.err.println("Failed!");
         System.exit(1);
       }
}
```

We test it by executing:

```
make sample N=10  TOOL=jmop    SPEC=../specs/process/mop/threadslice.mop
make sample N=10  TOOL=marq    SPEC=../specs/process/marq/SliceThread.aj
```
You should now see that verdicts are consistently *true*.
The result line should be:

```
T:10 F:0 Deadlock:0 U:0
```

We note that this works **in this case only**, as there is one iterator per thread.
However, it illustrates how we can perform *per-thread* monitoring.

### Per-thread Monitor per Iterator (Slicing on Threads and Iterator)

Of course, we can also slice using **two** parameters: *thread* and *iterator*.

We illustrate it in [Java-MOP](../specs/process/mop/bothslice.mop) and [MarQ](../specs/process/marq/SliceBoth.aj).

```
HasNext(Iterator i, Thread t) {

       event hasnext before(Iterator i, Thread t) :
             thread(t)
             && call(* Iterator.hasNext())
             && target(i) {}

       event next before(Iterator i, Thread t) :
             thread(t)
             && call(* Iterator.next())
             && target(i) {}

       fsm :
           start [
               next     -> unsafe
               hasnext  -> safe
           ]
           safe [
               next     -> start
               hasnext  -> safe
           ]
           unsafe [
               next     -> unsafe
               hasnext  -> unsafe
           ]

       alias match = unsafe

       @match {
         System.err.println("Failed!");
         System.exit(1);
       }
}
```
We test it by executing:

```
make sample N=10  TOOL=jmop    SPEC=../specs/process/mop/bothslice.mop
make sample N=10  TOOL=marq    SPEC=../specs/process/marq/SliceBoth.aj
```
You should now see that verdicts are consistently *true*.
The result line should be:

```
T:10 F:0 Deadlock:0 U:0
```

### Note on Flag *perthread*
You can also try running the *perthread* flag for Java-MOP, however we are not sure why it does not work consistently, even when explicitly slicing.
You can try for various levels of slicing ([iterator](../specs/process/mop/itersliceflag.mop), [thread](../specs/process/mop/threadsliceflag.mop), [both](../specs/process/mop/bothsliceflag.mop)).

Occasionally, at least one will display a *false* verdict (you will need more than 10 executions).

```
make sample N=100  TOOL=jmop    SPEC=../specs/process/mop/itersliceflag.mop
make sample N=100  TOOL=jmop    SPEC=../specs/process/mop/threadsliceflag.mop
make sample N=100  TOOL=jmop    SPEC=../specs/process/mop/bothsliceflag.mop
```
